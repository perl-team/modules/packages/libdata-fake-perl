Source: libdata-fake-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libimport-into-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libtest-deep-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libtext-lorem-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdata-fake-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdata-fake-perl.git
Homepage: https://metacpan.org/release/Data-Fake
Rules-Requires-Root: no

Package: libdata-fake-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libimport-into-perl,
         libscalar-list-utils-perl,
         libtext-lorem-perl
Description: module for generating fake structured data for testing
 Data::Fake generates randomized, fake structured data using declarative
 syntax.
 .
 Data::Fake is built on higher-order programming principles. It provides users
 with "factory" functions, which create "generator" functions for specific
 pieces of data.
 .
 Wherever randomized, fake data is desired, a generator code reference is used
 as a placeholder for the desired output. Each time the top-level generator is
 run, nested generators are recursively run to turn placeholders into the
 desired randomized data.
